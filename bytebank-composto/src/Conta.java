

public class Conta {
	private double saldo;
	int agencia = 42;
	int numero;
	Cliente titular = new Cliente();;
	
	public void deposita(double valor) {
		this.saldo += valor;
	}
	
	public boolean saca(double valor) {
		if(this.saldo - valor < 0) {
			return false;
		} else {
			this.saldo -= valor;
			return true;
		}
	}
	
	public boolean transfere(Conta contaDestino, Double valor) {
		if(this.saca(valor)) {
			contaDestino.deposita(valor);
			return true;
		} else {
			return false;
		}
	}
	
	public double pegaSaldo() {
		return this.saldo;
	}
}
