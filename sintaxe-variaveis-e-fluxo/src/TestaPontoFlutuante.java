
public class TestaPontoFlutuante {
	public static void main(String [] args) {
		
		double salario = 3750.40;
		
		System.out.println("Meu salário é: " + salario);
		
		double horasExtras;
		horasExtras = (40 * 20.5);
		
		System.out.println("Minhas horas extras deram: " + horasExtras + " em reais.");
		
		System.out.println("Meu salário final é: " + (salario + horasExtras));
		
		int divisaoInteiro = 5 / 2;
		System.out.println("A Divisão de Inteiro deu: " + divisaoInteiro);
		double divisaoDoubleErrada = 5 / 2;
		System.out.println("A Divisão de Inteiro deu: " + divisaoDoubleErrada);
		double divisaoDoubleCerta = 5.0 / 2;
		System.out.println("A Divisão de Inteiro deu: " + divisaoDoubleCerta);
	}
}
