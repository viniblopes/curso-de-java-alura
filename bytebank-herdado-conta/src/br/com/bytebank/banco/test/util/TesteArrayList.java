package br.com.bytebank.banco.test.util;

import java.util.ArrayList;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteArrayList {

	public static void main(String[] args) {

		ArrayList<Conta> lista = new ArrayList<Conta>();

//		List<Conta> lista = new Vector<Conta>();
		
        Conta cc = new ContaCorrente(22, 11);
        lista.add(cc);

        Conta cc2 = new ContaCorrente(22, 22);
        lista.add(cc2);

        boolean existe = lista.get(0).equals(cc2); //novo

        System.out.println("Já existe? " + existe);

//        for(int i = 0; i < lista.size(); i++) {
//            Object oRef = lista.get(i);
//            System.out.println(oRef);
//        }

        System.out.println("----------");

        for(Conta oRef : lista) {
            System.out.println(oRef);
        }



	}

}
