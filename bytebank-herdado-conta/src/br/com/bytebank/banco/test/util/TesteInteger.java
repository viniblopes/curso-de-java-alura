package br.com.bytebank.banco.test.util;

import java.util.ArrayList;
import java.util.List;

public class TesteInteger {

	public static void main(String[] args) {
		
		int[] idades = new int[5];
        String[] nomes = new String[5];

        int idade = 29; //Integer
        Integer idadeRef = Integer.valueOf(29);
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(idade); //Autoboxing
        numeros.add(idadeRef);
        
        String s = args[0];//"10"

//        Integer numero = Integer.valueOf(s);
        
        int numero = Integer.parseInt(s);
        System.out.println(numero);
        
        Integer ref = Integer.valueOf("3");
        ref++;
        System.out.println(ref);
        
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        System.out.println(Integer.BYTES);
        
        
        for(Object obj : numeros) {
        	System.out.println(obj);
        }
	}
}
